import java.util.*;

class SentenceProcessor {
    
    /** 
     * This method help you remove duplicated words and generate a set 
     * of unique words from s. Note that the words are case-sensative and
     * the sequence of s must not be changed. Therefore, I use LinledHashSet
     * to implement it.
     * @param s the string that you want to remove duplicated words
     * @return after removing duplicated words
     */
    public String removeDuplicatedWords(String s){
        String[] t = s.split(" ");
        LinkedHashSet hs = new LinkedHashSet();
        for (String i : t){
            hs.add(i);
        }
        return String.join(" ", hs);
    }
    
    /** 
     * Since the method needs to replace a "word" instead of a
     * substring, I use regex to as a parameter to a string method
     * replaceAll. "\\b" means a word boundary. 
     * reference: 
     * {@link https://docs.oracle.com/javase/1.5.0/docs/api/java/util/regex/Pattern.html}
     * @param regex the word you want to replace from
     * @param replacement the word you want to replace to
     * @param s the sentence that you want to execute replaceWord
     * @return modified s with string regex replaced by string replacement
     */
    public String replaceWord(String regex, String replacement, String s) {
        return s.replaceAll("\\b"+ regex +"\\b", replacement);
    }   
}
