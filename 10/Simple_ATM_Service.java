public class Simple_ATM_Service implements ATM_Service{
	
    /** 
     * @param account the account object
     * @param money the amount of money to check
     * @return boolean whether the account has enough balance
     * @throws ATM_Exception
     */
	@Override
	public boolean checkBalance(Account account, int money) throws ATM_Exception{
		if(account.getBalance() < money){
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.BALANCE_NOT_ENOUGH);
		}
        return true;
	}
	
    /** 
     * @param money the money to check
     * @return boolean whether the amount of money is multiple of 1000
     * @throws ATM_Exception
     */
	@Override
	public boolean isValidAmount(int money) throws ATM_Exception{
		if(money % 1000 != 0){
			throw new ATM_Exception(ATM_Exception.ExceptionTYPE.AMOUNT_INVALID);
		}
		return true;
	}
    
    /** 
     * @param account the account object
     * @param money the amount of money to withdraw
     * @return void
     */
    @Override
	public void withdraw(Account account, int money){
		try{
			checkBalance(account, money);
			isValidAmount(money);
			account.setBalance(account.getBalance() - money);
		}
		catch(ATM_Exception e){
			System.out.println(e.getMessage());
		}
		System.out.println("updated balance : " + account.getBalance());
	}
}