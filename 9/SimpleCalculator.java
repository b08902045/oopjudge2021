public class SimpleCalculator {
    // There are five kinds of unknown command exceptions: 
    // 1. the operator is any operator symbol other than +, -, *, or /. 
    // 2. the value can’t be parsed as a real number. 
    // 3. the operator and the value are both invalid.
    // 4. the command is not formed by exact one operator and one value separated by one space. 
    // 5. the command indicates dividing by zero. 
    // An UnknownCmdException will be thrown if one of these exceptions described above happens. 
    // You need to pass the error message to UnknownCmdException class in order to figure out what 
    // happens. The system will call exception.getMessage() to check it out.
    private char operator;
    private double value, result;
    private int count;
    private boolean isEnded;
    
    
    /** 
     * @param cmd: the command to calculate including operator and operand
     * @return void
     * @throws UnknownCmdException
     */
    public void calResult(String cmd) throws UnknownCmdException {
		String[] cmdArr;
		cmdArr = cmd.split(" ");
		// 4. the command is not formed by exact one operator and one value separated by one space. 
		if(cmdArr.length != 2)
			throw new UnknownCmdException("Please enter 1 operator and 1 value separated by 1 space");
		
        boolean operatorValid = true;
		boolean valueValid = true;
		// 1. the operator is any operator symbol other than +, -, *, or /. 
		if (cmdArr[0].length() != 1 || !(cmdArr[0].charAt(0) == '+' || cmdArr[0].charAt(0) == '-'
         || cmdArr[0].charAt(0) == '*' || cmdArr[0].charAt(0) == '/'))
			operatorValid = false;
		else
			operator = cmdArr[0].charAt(0);

		// 2. the value can’t be parsed as a real number. 
		try {
			value = Double.parseDouble(cmdArr[1]);
		} catch (NumberFormatException e) {
			valueValid = false;
		}
		// throw error message
		if(!operatorValid && !valueValid)
			throw new UnknownCmdException(cmdArr[0] + " is an unknown operator and " + cmdArr[1] + " is an unknown value");
		else if(!operatorValid)
			throw new UnknownCmdException(cmdArr[0] + " is an unknown operator");
		else if(!valueValid)
			throw new UnknownCmdException(cmdArr[1] + " is an unknown value");	
		
        // 5. the command indicates dividing by zero. 
		if(operator == '/' && value == 0)
			throw new UnknownCmdException("Can not divide by 0");

		switch (operator) {
		case '+':
			result += value;
			break;
		case '-':
			result -= value;
			break;
		case '*':
			result *= value;
			break;
		case '/':
			result /= value;
			break;
		}
		count++;
	}
	
	public 
    /** 
     * different count number will lead to different result
     * @return the message of the result of this step
     */
    String getMsg() {
		String resultStr = String.format("%.2f", this.result);
		String valueStr = String.format("%.2f", this.value);
		if (isEnded)
			return ("Final result = " + resultStr);
		if (count == 0)
			return ("Calculator is on. Result = " + resultStr);
		else if (count == 1)
			return ("Result " + operator + " " + valueStr + " = " + resultStr + ". " + "New result = " + resultStr);
		else
			return ("Result " + operator + " " + valueStr + " = " + resultStr + ". " + "Updated result = " + resultStr);
	}

	public 
    /** 
     * handle the end of calculating
     * @param cmd: the command of ending calculation is 'R' or 'r'
     * @return the boolean of whether the calculation comes to the end or not
     */
    boolean endCalc(String cmd) {
		if (cmd.length() == 1 && (cmd.charAt(0) == 'R' || cmd.charAt(0) == 'r')) {
			isEnded = true;
			return true;
		} else
			return false;
	}

}
