
public class PizzaOrder {
    int num;
    Pizza Pizza1, Pizza2, Pizza3;
    /** 
     * @param numberPizzas
     * the number of pizzas
     * @return false if the number of pizza is not in the range of 1 and 3
     */
    public boolean setNumberPizzas(int numberPizzas){
        num = numberPizzas;
        return numberPizzas <= 3 && numberPizzas >= 1;
    }
    
    /** 
     * @param pizza1 the first pizza to set
     * @return 
     */
    public void setPizza1(Pizza pizza1) {
        Pizza1 = pizza1;
    }
    
    /** 
     * @param pizza2 the second pizza to set
     * @return 
     */
    public void setPizza2(Pizza pizza2) {
        Pizza2 = pizza2;
    }
    
    /** 
     * @param pizza3 the third pizza to set
     * @return 
     */
    public void setPizza3(Pizza pizza3) {
        Pizza3 = pizza3;
    }
    
    /** 
     * @return the total cost of the ordered pizza
     */
    double calcTotal(){
        if (num == 1)
            return Pizza1.calcCost();
        else if (num == 2)
            return Pizza1.calcCost() + Pizza2.calcCost();
        return Pizza1.calcCost() + Pizza2.calcCost() + Pizza3.calcCost();
    }
}