
public class IsLeapYear {
    
    /** 
     * @param a: the given year
     * @return is leap year or not
     */
    public static boolean determine(int a){
        return a % 400 == 0 || (a % 4 == 0 && a % 100 != 0);
    }
}
