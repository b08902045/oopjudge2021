
public class Pizza {
    private String Size;
    private int NumberOfCheese, NumberOfPepperoni, NumberOfHam;
    
    /** 
     * @return the pizza instance with default constuctor that set size to small, 
     * the other variables to 1
     */
    
    /** 
     * @return the pizza instance with desired values
     */
    public Pizza(){
        this.Size = "small";
        this.NumberOfCheese = this.NumberOfHam = this.NumberOfPepperoni = 1;
    }
    public Pizza(String Size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam){
        this.Size = Size;
        this.NumberOfCheese = NumberOfCheese; this.NumberOfPepperoni = NumberOfPepperoni; this.NumberOfHam = NumberOfHam;
    }
    
    /** 
     * @return the instance variable string size
     */
    public String getSize() {
        return Size;
    }
    
    /** 
     * @param size the string of size one wants to set, e.g. "small" or "medium" or "large"
     * @return void 
     */
    public void setSize(String size) {
        Size = size;
    }
    
    /** 
     * @return the value of NumberOfCheese
     */
    public int getNumberOfCheese() {
        return NumberOfCheese;
    }
    
    /** 
     * @param numberOfCheese integer of numberOfCheese
     * @return void
     */
    public void setNumberOfCheese(int numberOfCheese) {
        NumberOfCheese = numberOfCheese;
    }
    
    /** 
     * @return the value of NumberOfPepperoni
     */
    public int getNumberOfPepperoni() {
        return NumberOfPepperoni;
    }
    
    /** 
     * @param numberOfPepperoni integer of numberOfPepperoni
     * @return void
     */
    public void setNumberOfPepperoni(int numberOfPepperoni) {
        NumberOfPepperoni = numberOfPepperoni;
    }
    
    /** 
     * @return the value of NumberOfHam
     */
    public int getNumberOfHam() {
        return NumberOfHam;
    }
    
    /** 
     * @param numberOfHam integer of numberOfHam
     * @return void
     */
    public void setNumberOfHam(int numberOfHam) {
        NumberOfHam = numberOfHam;
    }

    
    /** 
     * @return the cost of the instance pizza, where the cost is determined by:
     * Small: $10 + $2 per topping
     * Medium: $12 + $2 per topping
     * Large: $14 + $2 per topping
     */
    public double calcCost(){
        if (Size == "small")
            return 10 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else if (Size == "medium")
            return 12 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else if(Size == "large")
            return 14 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else
            return 0;
    }
    
    /** 
     * @param p the instance pizza to overload the equal method
     * @return the boolean of whether equal or not
     */
    public boolean equals(Pizza p){
        return p.Size == Size && p.NumberOfCheese == NumberOfCheese && p.NumberOfHam == NumberOfHam && p.NumberOfPepperoni == NumberOfPepperoni;
    }
    
    /** 
     * @return the string format of a pizza with each variable. The output format is as below:
     * "size = small, numOfCheese = 0, numOfPepperoni = 0, numOfHam = 0"
     */
    public String toString(){
        return String.format("size = %s, numOfCheese = %d, numOfPepperoni = %d, numOfHam = %d", Size, NumberOfCheese, NumberOfPepperoni, NumberOfHam);
    }
}
