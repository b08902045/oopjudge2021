import java.util.HashSet;
import java.util.Set;

public class SimpleArrayList {
	private Integer[] array = new Integer[100000];
	private int size = 0;
	/**
	 * constructor
	 */
	public SimpleArrayList() {
		return;
	}
	/**
	 * @param size Setting the size of arraylist to size.
	 */
	public SimpleArrayList(int size) {
		this.size = size;
		for(int i = 0; i < size; i++)
			array[i] = 0;
	}
	/**
	 * @param i Append an Integer i to the arrayList
	 */
	public void add(Integer i) {
		array[size++] = i;
	}
	/**
	 * @param index get Integer in the index of the arraylist
	 * @return the element at the specified position in this array. 
     * If the specified position is out of range of the array, returns null.
	 */
	public Integer get(int index) {
		return (index >= size)? null : array[index];
	}
	/**
     * replaces the element at the specified position in this array with the 
     * specified element, and returns the original element at that specified 
     * position. If the specified position is out of range of the array, 
     * returns null.
	 * @param index the index where to set
	 * @param element set the ArrayList[index] = element
	 * @return the original element if success, otherwise null
	 */
	public Integer set(int index, Integer element) {
		if(index >= size)
            return null;
		Integer temp = array[index];
		array[index] = element;
		return temp;
	}
	/**
     * If a null element is at the specified position, returns false; otherwise 
     * removes it and returns true. Shifts any subsequent elements to the left if 
     * removes succesfully.
	 * @param index the index where to remove
	 * @return whether successfully remove the element
	 */
	public boolean remove(int index) {
		if(array[index] == null)
            return false;
		for(int i = index; i < size-1; i++) {
			array[i] = array[i+1];
		}
		size--;
		return true;
	}
	/**
	 * removes all of the elements from this array.
	 */
	public void clear() {
		size = 0;
	}
	/**
	 * @return the number of elements in this array.
	 */
	public int size() {
		return size;
	}
	/**
	 * @param l The ArrayList to retain
	 * @return false if none of the elements is removed
	 */
	public boolean retainAll(SimpleArrayList l) {
        if(l.size() == 0)   return false;

        Set<Integer> set = new HashSet<>();
        Integer[] retained = new Integer[100000];
        int p = 0;

        for(int i = 0; i < l.size(); i++)
            set.add(l.get(i));
        for(int i = 0; i < this.size; i++){
            if(set.contains(array[i])){
                retained[p++] = array[i];
            }
        }
        array = retained;
        int old = size;
        size = p;

        return !(old == p);
	}
}
