import java.util.*;

public class main {
    public static void main(String[] args) {
        Pizza pizza = new Pizza("large", 3, 1, 5);
        System.out.println(pizza.getSize());
        System.out.println(pizza.getNumberOfCheese());
        System.out.println(pizza.getNumberOfPepperoni());
        System.out.println(pizza.getNumberOfHam());
        pizza = new Pizza();
        pizza.setSize("medium");
        pizza.setNumberOfCheese(2);
        pizza.setNumberOfPepperoni(4);
        pizza.setNumberOfHam(1);
        System.out.println(pizza.getSize());
        System.out.println(pizza.getNumberOfCheese());
        System.out.println(pizza.getNumberOfPepperoni());
        System.out.println(pizza.getNumberOfHam());
        System.out.println(pizza.calCost());
        System.out.println(pizza.toString());
        System.out.println(pizza.equals(new Pizza("large", 2, 4, 1)));
        System.out.println(pizza.equals(new Pizza()));
        System.out.println(pizza.equals(new Pizza("medium", 2, 4, 1)));
    }
}

class Pizza {
    String Size;
    int NumberOfCheese, NumberOfPepperoni, NumberOfHam;
    public Pizza(){
        this.Size = "small";
        this.NumberOfCheese = this.NumberOfHam = this.NumberOfPepperoni = 1;
    }
    public Pizza(String Size, int NumberOfCheese, int NumberOfPepperoni, int NumberOfHam){
        this.Size = Size;
        this.NumberOfCheese = NumberOfCheese; this.NumberOfPepperoni = NumberOfPepperoni; this.NumberOfHam = NumberOfHam;
    }
    public String getSize() {
        return Size;
    }
    public void setSize(String size) {
        Size = size;
    }
    public int getNumberOfCheese() {
        return NumberOfCheese;
    }
    public void setNumberOfCheese(int numberOfCheese) {
        NumberOfCheese = numberOfCheese;
    }
    public int getNumberOfPepperoni() {
        return NumberOfPepperoni;
    }
    public void setNumberOfPepperoni(int numberOfPepperoni) {
        NumberOfPepperoni = numberOfPepperoni;
    }
    public int getNumberOfHam() {
        return NumberOfHam;
    }
    public void setNumberOfHam(int numberOfHam) {
        NumberOfHam = numberOfHam;
    }

    public double calCost(){
        if (Size == "small")
            return 10 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else if (Size == "medium")
            return 12 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else if(Size == "large")
            return 14 + 2*(NumberOfCheese + NumberOfPepperoni + NumberOfHam);
        else
            return 0;
    }
    public boolean equals(Pizza p){
        return p.Size == Size && p.NumberOfCheese == NumberOfCheese && p.NumberOfHam == NumberOfHam && p.NumberOfPepperoni == NumberOfPepperoni;
    }
    public String toString(){
        return String.format("size = %s, numOfCheese = %d, numOfPepperoni = %d, numOfHam = %d", Size, NumberOfCheese, NumberOfPepperoni, NumberOfHam);
    }
}
