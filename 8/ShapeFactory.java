public class ShapeFactory {
	enum Type {
		Circle,
        Square,
        Triangle
	}
    /**
     * @param type of shape
     * @param length
     * create different shape
     */
	public Shape createShape(ShapeFactory.Type shapeType, double length){
		switch(shapeType){
            case Circle:
                    return new Circle(length);
            case Square:
                    return new Square(length);
            case Triangle:
                    return new Triangle(length);
            default:
                    return null;
		}
	}
}