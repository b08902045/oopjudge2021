public class Triangle extends Shape {
	/**
     * @param length
     * constructor
     */
	public Triangle(double length) {
		super(length);
	}
    /**
     * set length
     */
	public void setLength(double length){
		this.length = length;
		return;
	}
	/*
	 * @return the calculated area
	 */
	public double getArea() {
        return (double)Math.round(Math.sqrt(3)/4 * length * length * 100) / 100;
	}
	/*
	 * @return the calculated perimeter
	 */
	public double getPerimeter() {
        return (double)Math.round(length * 3 * 100) / 100;
	}
}