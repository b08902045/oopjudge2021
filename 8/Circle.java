public class Circle extends Shape {
	/**
     * @param length
     * constructor
     */
	public Circle(double length) {
		super(length);
	}
    /**
     * set length
     */
	public void setLength(double length) {
        this.length = length;
        return;
	}
	/*
	 * @return the calculated area
	 */
	public double getArea() {
        return (double)Math.round(length/2 * length/2 * Math.PI * 100) / 100;
	}
	/*
	 * @return the calculated perimeter
	 */
	public double getPerimeter() {
        return (double)Math.round(length * Math.PI * 100) / 100;
	}
}