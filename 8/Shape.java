public abstract class Shape {
	protected double length;
	/** 
	 * @param length
	 * @return 
	 */
	public Shape(double length){
		this.length=length;
	}
	
	/** 
	 * set length
	 */
	public abstract void setLength(double length);
	
	/** 
	 * get area
	 */
	public abstract double getArea();
	
	/** 
	 * get perimeter
	 */
	public abstract double getPerimeter();
	
	/** 
	 * @return the information
	 */
	public String getInfo(){
		return "Area = " + getArea()+ ", Perimeter = " + getPerimeter();
	}
}