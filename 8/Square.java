public class Square extends Shape {
	/**
     * @param length
     * constructor
     */
	public Square(double length) {
		super(length);
	}
    /**
     * set length
     */
	public void setLength(double length) {
		this.length = length;
		return;
	}
	/*
	 * @return the calculated area
	 */
	public double getArea() {
        return (double)Math.round(length * length * 100) / 100;
	}
	/*
	 * @return the calculated perimeter
	 */
	public double getPerimeter() {
        return (double)Math.round(length * 4 * 100) / 100;
	}
}